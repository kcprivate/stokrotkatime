import React from 'react';
import {Animated, Easing} from 'react-native';
import LottieView from 'lottie-react-native';

export default class Animation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: new Animated.Value(0),
    };
  }

  componentDidMount() {
    Animated.loop(
      Animated.timing(this.state.progress, {
        toValue: 1,
        duration: 2000,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();
  }

  render() {
    return (
      <LottieView
        source={require('./images/loading.json')}
        progress={this.state.progress}
      />
    );
  }
}
