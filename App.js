import React from 'react';
import {View, Text, TouchableOpacity, Switch, Appearance} from 'react-native';
import tailwind from 'tailwind-rn';
import LinearGradient from 'react-native-linear-gradient';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';

import Animation from './src/Animation';
import Sad from './src/images/sad.svg';
import Smile from './src/images/smile.svg';
import Wrr from './src/images/wrr.svg';
import Warning from './src/images/warning.svg';
import Settings from './src/images/settings.svg';
import Auto from './src/images/auto.svg';
import Dark from './src/images/dark.svg';
import Light from './src/images/light.svg';

class InfoField extends React.PureComponent {
  renderTimer = () => {
    if (this.props.timer === null) {
      return (
        <View style={tailwind('flex items-center justify-center py-6 ml-6')}>
          <Warning
            width={32}
            height={32}
            fill={this.props.bg === '#252525' ? '#DAE0EA' : '#677794'}
          />
        </View>
      );
    } else {
      return (
        <View
          style={[
            tailwind('flex items-center justify-center py-6 border-r ml-1'),
            {flex: 1.5, borderColor: '#DAE0EA'},
          ]}>
          <Text
            style={[
              tailwind('font-bold text-xl leading-5'),
              this.props.bg === '#252525'
                ? {color: '#DAE0EA'}
                : {color: '#677794'},
              {fontFamily: 'Lato-Black', fontWeight: '700'},
            ]}>
            {this.props.timer}
          </Text>
        </View>
      );
    }
  };

  render() {
    return (
      <View
        style={[
          tailwind('flex flex-row w-full items-center rounded-xl py-2 mb-4'),
          {backgroundColor: this.props.bg},
        ]}>
        {this.renderTimer()}
        <Text
          style={[
            tailwind('pl-6'),
            {flex: 3},
            this.props.bg === '#252525'
              ? {color: '#DAE0EA'}
              : {color: '#677794'},
            this.props.timer === null &&
              tailwind('font-normal text-sm leading-4 mr-4'),
            {fontFamily: 'Lato-Regular'},
          ]}>
          {this.props.text}
        </Text>
      </View>
    );
  }
}

class ThemeType extends React.PureComponent {
  renderIcon = () => {
    let color = this.props.textColor === '#FFFFFF' ? '#677794' : '#DAE0EA';
    switch (this.props.type) {
      case 'Light':
        return (
          <Light
            width={this.props.size}
            height={this.props.size}
            color={color}
          />
        );
      case 'Dark':
        return (
          <Dark
            width={this.props.size}
            height={this.props.size}
            color={color}
          />
        );
      case 'Auto':
        return (
          <Auto
            width={this.props.size}
            height={this.props.size}
            color={color}
          />
        );
    }
  };
  render() {
    return (
      <View style={tailwind('flex flex-row w-full justify-between py-2')}>
        <View style={tailwind('flex flex-row items-center')}>
          {this.renderIcon()}
          <Text
            style={[
              tailwind('font-normal text-sm ml-2.5'),
              this.props.textColor === '#FFFFFF'
                ? {color: '#677794'}
                : {color: '#DAE0EA'},
              {fontFamily: 'Lato-Regular'},
            ]}>
            {this.props.text}
          </Text>
        </View>
        <Switch
          onChange={() => this.props.changeTheme(this.props.type)}
          value={this.props.enabled}
        />
      </View>
    );
  }
}

class App extends React.PureComponent {
  state = {
    mainBg: '#F4F5F9',
    infoFieldBg: '#FFFFFF',
    modalBg: '#FFFFFF',
    timeLeftToEndBreak: '',
    timeLeftToNextBreak: '',
    timeLeftToNext2Break: '',
    themeType: 'auto',
    timePassed: false,
    settingsOpen: false,
    canGo: true,
    gestureName: 'none',
    badTimes: [
      {startTime: '08:45:00', endTime: '08:50:00'},
      {startTime: '09:35:00', endTime: '09:40:00'},
      {startTime: '10:25:00', endTime: '10:35:00'},
      {startTime: '11:20:00', endTime: '11:30:00'},
      {startTime: '12:15:00', endTime: '12:35:00'},
      {startTime: '13:20:00', endTime: '13:35:00'},
      {startTime: '14:20:00', endTime: '14:25:00'},
      {startTime: '15:10:00', endTime: '15:15:00'},
      {startTime: '16:00:00', endTime: '16:05:00'},
      {startTime: '16:50:00', endTime: '16:55:00'},
    ],
  };

  componentDidMount() {
    SplashScreen.hide();
    this.getData();
    this.renderBgColor();
    setTimeout(() => {
      this.setState({timePassed: true});
    }, 3000);
    this.timeout();
  }

  componentDidUpdate() {
    this.renderBgColor();
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  storeData = async value => {
    try {
      await AsyncStorage.setItem('@theme', value);
    } catch (e) {
      console.error(e);
    }
  };

  getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@theme');
      if (value !== null) {
        this.setState({themeType: value});
      }
    } catch (e) {
      console.error(e);
    }
  };

  onSwipeDown() {
    console.log('You swiped down!');
  }

  startSwiping(gestureName) {
    const {SWIPE_DOWN} = swipeDirections;
    this.setState({gestureName: gestureName});
    switch (gestureName) {
      case SWIPE_DOWN:
        this.setState({settingsOpen: false});
        break;
    }
  }

  msToTime = s => {
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;
    return Math.abs(hrs) + ':' + Math.abs(mins) + ':' + Math.abs(secs);
  };

  formatDate = (time, nextDay) => {
    let arr = time.split(':');
    let date = new Date();
    if (nextDay) {
      date.setDate(date.getDate() + 1);
      date.setHours(arr[0], arr[1], arr[2]);
    } else {
      date.setHours(arr[0], arr[1], arr[2]);
    }
    return date;
  };

  formatNumber = number => {
    let result = 0;
    if (number < 10) {
      result = '0' + number;
    } else {
      result = number;
    }

    return result;
  };

  getOnlyTime = date => {
    let actualTime =
      date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    return actualTime;
  };

  checkTimev2 = currentTime => {
    const {badTimes} = this.state;
    for (let i = 0; i < badTimes.length; ++i) {
      let breakStart = this.formatDate(badTimes[i].startTime);
      let breakEnd = this.formatDate(badTimes[i].endTime);
      let nextBreakStart = '',
        nextBreakEnd = '',
        next2BreakStart = '',
        next2BreakEnd = '';
      let timeLeftToEndBreak = '',
        timeLeftToNextBreak = '',
        timeLeftToNext2Break = '';

      if (currentTime >= breakStart && currentTime <= breakEnd) {
        this.setState({canGo: false});

        if (i !== badTimes.length - 1) {
          nextBreakStart = this.formatDate(badTimes[i + 1].startTime);
          nextBreakEnd = this.formatDate(badTimes[i + 1].endTime);
        } else {
          nextBreakStart = this.formatDate(badTimes[0].startTime, true);
          nextBreakEnd = this.formatDate(badTimes[0].endTime, true);
        }

        timeLeftToNextBreak = nextBreakStart - currentTime;
        timeLeftToEndBreak = breakEnd - currentTime;

        this.setState({timeLeftToEndBreak: timeLeftToEndBreak});
        this.setState({timeLeftToNextBreak: timeLeftToNextBreak});
      } else if (
        i !== badTimes.length - 1 &&
        currentTime >= breakEnd &&
        currentTime <= this.formatDate(badTimes[i + 1].startTime)
      ) {
        this.setState({canGo: true});

        if (i !== badTimes.length - 1) {
          nextBreakStart = this.formatDate(badTimes[i + 1].startTime);
          nextBreakEnd = this.formatDate(badTimes[i + 1].endTime);
          if (i + 1 !== badTimes.length - 1) {
            next2BreakStart = this.formatDate(badTimes[i + 2].startTime);
            next2BreakEnd = this.formatDate(badTimes[i + 2].endTime);
          } else {
            next2BreakStart = this.formatDate(badTimes[0].startTime, true);
            next2BreakEnd = this.formatDate(badTimes[0].endTime, true);
          }
        } else {
          nextBreakStart = this.formatDate(badTimes[0].startTime, true);
          nextBreakEnd = this.formatDate(badTimes[0].endTime, true);
          next2BreakStart = this.formatDate(badTimes[1].startTime, true);
          next2BreakEnd = this.formatDate(badTimes[1].endTime, true);
        }

        timeLeftToNextBreak = nextBreakStart - currentTime;
        timeLeftToNext2Break = next2BreakStart - currentTime;

        this.setState({timeLeftToNextBreak: timeLeftToNextBreak});
        this.setState({timeLeftToNext2Break: timeLeftToNext2Break});
      } else if (
        i === badTimes.length - 1 &&
        currentTime >= breakEnd &&
        currentTime <= this.formatDate(badTimes[0].startTime, true)
      ) {
        this.setState({canGo: true});

        if (i !== badTimes.length - 1) {
          nextBreakStart = this.formatDate(badTimes[i + 1].startTime);
          nextBreakEnd = this.formatDate(badTimes[i + 1].endTime);
          if (i + 1 !== badTimes.length - 1) {
            next2BreakStart = this.formatDate(badTimes[i + 2].startTime);
            next2BreakEnd = this.formatDate(badTimes[i + 2].endTime);
          } else {
            next2BreakStart = this.formatDate(badTimes[0].startTime, true);
            next2BreakEnd = this.formatDate(badTimes[0].endTime, true);
          }
        } else {
          nextBreakStart = this.formatDate(badTimes[0].startTime, true);
          nextBreakEnd = this.formatDate(badTimes[0].endTime, true);
          next2BreakStart = this.formatDate(badTimes[1].startTime, true);
          next2BreakEnd = this.formatDate(badTimes[1].endTime, true);
        }

        timeLeftToNextBreak = nextBreakStart - currentTime;
        timeLeftToNext2Break = next2BreakStart - currentTime;

        this.setState({timeLeftToNextBreak: timeLeftToNextBreak});
        this.setState({timeLeftToNext2Break: timeLeftToNext2Break});
      }
    }
  };

  timeout = () => {
    this.timer = setTimeout(() => {
      let currentDate = new Date();
      this.checkTimev2(currentDate);
      this.timeout();
    }, 250);
  };

  formatTimev2 = time => {
    let tempTime = time.split(':');
    let resultTime = '',
      hrs = '',
      min = '',
      sec = '';

    if (tempTime[0] < 10 && tempTime[0] > 0) {
      hrs = '0' + tempTime[0];
    } else if (tempTime[0] < 10 && tempTime[0] < 1) {
      hrs = '';
    } else {
      hrs = tempTime[0];
    }

    if (tempTime[1] < 10) {
      min = '0' + tempTime[1];
    } else {
      min = tempTime[1];
    }

    if (tempTime[2] < 10) {
      sec = '0' + tempTime[2];
    } else {
      sec = tempTime[2];
    }

    if (hrs === '') {
      resultTime = min + ':' + sec;
    } else {
      resultTime = hrs + ':' + min + ':' + sec;
    }

    return resultTime;
  };

  renderHeader = () => {
    const {timeLeftToNextBreak, canGo} = this.state;
    let buttonTitle = '';
    let buttonColors = [];
    let shadowColor = '';
    let icon;

    if (canGo) {
      let timer = this.msToTime(timeLeftToNextBreak);
      let tempTimerArray = timer.split(':');
      if (parseInt(tempTimerArray[0]) <= 0 && parseInt(tempTimerArray[1]) < 5) {
        buttonTitle = 'SZYBKO SZYBKO, MOŻE ZDĄŻYSZ!';
        buttonColors = ['#FD7900', '#D64000'];
        shadowColor = '#E27D20';
        icon = <Wrr width={36} height={36} />;
      } else {
        buttonTitle = 'MOŻNA WYDUPCAĆ!';
        buttonColors = ['#87CC41', '#4D9A00'];
        shadowColor = '#6EB429';
        icon = <Smile width={36} height={36} />;
      }
    } else {
      buttonTitle = 'SIEDŹ NA DUPIE!';
      buttonColors = ['#EB0000', '#BF0000'];
      shadowColor = '#B42929';
      icon = <Sad width={36} height={36} />;
    }

    return (
      <View
        style={[
          tailwind('flex flex-row w-full items-center mb-8'),
          {
            shadowColor: shadowColor,
            shadowOffset: {
              width: 0,
              height: 10,
            },
            shadowOpacity: 0.25,
            shadowRadius: 15,
            elevation: 4,
          },
        ]}>
        <LinearGradient
          colors={buttonColors}
          style={tailwind(
            'flex flex-row w-full py-11 pl-6 pr-16 items-center rounded-xl',
          )}>
          {icon}
          <Text
            style={[
              tailwind('text-2xl text-white ml-6'),
              {fontFamily: 'Lato-Regular'},
            ]}>
            {buttonTitle}
          </Text>
        </LinearGradient>
      </View>
    );
  };

  renderTimers = () => {
    const {
      timeLeftToEndBreak,
      timeLeftToNextBreak,
      timeLeftToNext2Break,
      canGo,
    } = this.state;
    let timer1,
      timer2,
      text1 = '',
      text2 = '',
      text3 =
        'Niezastosowanie się do powyższych zaleceń, grozi cięzkim uszczerbkiem na zdrowiu psychicznym oraz stratą czasu!';

    if (canGo) {
      timer1 = this.formatTimev2(this.msToTime(timeLeftToNextBreak));
      timer2 = this.formatTimev2(this.msToTime(timeLeftToNext2Break));
      text1 = 'zostało do najbliższej przerwy';
      text2 = 'rozpocznie się kolejna przerwa';
    } else {
      timer1 = this.formatTimev2(this.msToTime(timeLeftToEndBreak));
      timer2 = this.formatTimev2(this.msToTime(timeLeftToNextBreak));
      text1 = 'zostało do końca przerwy';
      text2 = 'rozpocznie się kolejna przerwa';
    }

    return (
      <>
        <InfoField timer={timer1} text={text1} bg={this.state.infoFieldBg} />
        <InfoField timer={timer2} text={text2} bg={this.state.infoFieldBg} />
        <InfoField timer={null} text={text3} bg={this.state.infoFieldBg} />
      </>
    );
  };

  renderSettingsButton = () => {
    return (
      <TouchableOpacity
        onPress={() => this.setState({settingsOpen: !this.state.settingsOpen})}
        style={[
          tailwind(
            'absolute bottom-0 right-0 p-4 mr-4 mb-11 bg-white rounded-full',
          ),
          this.state.mainBg === '#F4F5F9'
            ? {backgroundColor: '#FFFFFF'}
            : {backgroundColor: '#252525'},
        ]}>
        <Settings
          height={23}
          width={23}
          color={this.state.mainBg === '#F4F5F9' ? '#677794' : '#DAE0EA'}
        />
      </TouchableOpacity>
    );
  };

  changeTheme = type => {
    const tempType = type.toLowerCase();
    if (this.state.themeType === tempType) {
      this.setState({settingsOpen: false});
    } else {
      switch (tempType) {
        case 'light':
          this.storeData('light');
          this.setState({themeType: 'light'});
          break;
        case 'dark':
          this.storeData('dark');
          this.setState({themeType: 'dark'});
          break;
        case 'auto':
          this.storeData('auto');
          this.setState({themeType: 'auto'});
          break;
      }
    }
  };

  renderSettingsModal = () => {
    if (this.state.settingsOpen) {
      return (
        <Animatable.View
          animation="fadeIn"
          useNativeDriver={true}
          easing="ease-in-out"
          duration={250}
          style={tailwind(
            'absolute w-full h-full bottom-0 bg-black bg-opacity-70',
          )}>
          <GestureRecognizer
            onSwipe={direction => this.startSwiping(direction)}
            onSwipeDown={() => this.onSwipeDown()}
            config={{velocityThreshold: 0.3, directionalOffsetThreshold: 80}}
            style={tailwind('flex-1')}>
            <Animatable.View
              animation="fadeInUp"
              useNativeDriver={true}
              easing="ease-in-out"
              duration={250}
              style={[
                tailwind(
                  'flex absolute w-full bottom-0 items-center justify-start h-1/3 p-4 rounded-t-3xl',
                ),
                {backgroundColor: this.state.modalBg},
              ]}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({settingsOpen: !this.state.settingsOpen})
                }
                style={[
                  tailwind('w-12 h-1 rounded-3xl'),
                  {backgroundColor: '#DAE0EA'},
                ]}
              />
              <View style={tailwind('flex w-full items-start')}>
                <Text
                  style={[
                    tailwind('my-2.5 text-left text-sm font-normal'),
                    this.state.modalBg === '#FFFFFF'
                      ? {color: '#000000'}
                      : {color: '#ffffff'},
                    {fontFamily: 'Lato-Regular'},
                  ]}>
                  Ustawienia motywu
                </Text>
                <View>
                  <ThemeType
                    type={'Light'}
                    size={32}
                    changeTheme={this.changeTheme}
                    enabled={this.state.themeType === 'light'}
                    textColor={this.state.modalBg}
                    text={'Jasny'}
                  />
                  <ThemeType
                    type={'Dark'}
                    size={32}
                    changeTheme={this.changeTheme}
                    enabled={this.state.themeType === 'dark'}
                    textColor={this.state.modalBg}
                    text={'Ciemny'}
                  />
                  <ThemeType
                    type={'Auto'}
                    size={32}
                    changeTheme={this.changeTheme}
                    enabled={this.state.themeType === 'auto'}
                    textColor={this.state.modalBg}
                    text={'Automatycznie'}
                  />
                </View>
              </View>
            </Animatable.View>
          </GestureRecognizer>
        </Animatable.View>
      );
    }
  };

  renderBgColor = () => {
    switch (this.state.themeType) {
      case 'light':
        this.setState({
          mainBg: '#F4F5F9',
          modalBg: '#FFFFFF',
          infoFieldBg: '#FFFFFF',
        });
        break;
      case 'dark':
        this.setState({
          mainBg: '#000000',
          modalBg: '#242424',
          infoFieldBg: '#252525',
        });
        break;
      case 'auto':
        const colorScheme = Appearance.getColorScheme();
        if (colorScheme === 'light' || colorScheme === null) {
          this.setState({
            mainBg: '#F4F5F9',
            modalBg: '#FFFFFF',
            infoFieldBg: '#FFFFFF',
          });
          break;
        } else {
          this.setState({
            mainBg: '#000000',
            modalBg: '#242424',
            infoFieldBg: '#252525',
          });
          break;
        }
    }
  };

  render() {
    return (
      <View
        style={[
          tailwind('flex-1 justify-start items-center'),
          {backgroundColor: this.state.mainBg},
        ]}>
        <View style={tailwind('flex-1 justify-start mx-2 pt-16 items-center')}>
          {this.renderHeader()}
          {this.renderTimers()}
          {this.renderSettingsButton()}
        </View>
        {this.renderSettingsModal()}
      </View>
    );
  }
}

export default App;
